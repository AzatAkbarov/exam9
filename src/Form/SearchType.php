<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('q',TextType::class, [
            'label' => false,
            'attr' => [
                'placeholder' => 'Что ищем?'
            ]
        ])
            ->add('sort', ChoiceType::class, [
                'label' => false,
                'placeholder' => 'Выберите один из вариантов',
                'choices' => [
                    'По релевантности (relevance)' => 'relevance',
                    'Горячие (hot)' => 'hot',
                    'В топе (top)' => 'top',
                    'Новые (new)' => 'new',
                    'Комментируемые (comments)' => 'comments'
                ]
            ])
            ->add('limit', ChoiceType::class, [
                'label' => false,
                'placeholder' => 'Сколько?',
                'choices' => [
                    '8шт' => 8,
                    '12шт' => 12,
                    '16шт' => 16,
                    '50шт' => 50,
                    '100шт' => 100
                ]
            ])
            ->add('search', SubmitType::class, [
                'label' => 'Искать'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_search_type';
    }

}