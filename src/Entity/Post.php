<?php

namespace App\Entity;

class Post
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $image;


    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $author;


    /**
     * @var string
     */
    private $created_uts;

    private $userQuantity;

    /**
     * @param string $id
     * @return Post
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $image
     * @return Post
     */
    public function setImage(string $image): Post
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param mixed $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $author
     * @return Post
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $created_uts
     * @return Post
     */
    public function setCreatedUts($created_uts)
    {
        $this->created_uts = $created_uts;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedUts()
    {
        return $this->created_uts;
    }

    /**
     * @param mixed $userQuantity
     * @return Post
     */
    public function setUserQuantity($userQuantity)
    {
        $this->userQuantity = $userQuantity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserQuantity()
    {
        return $this->userQuantity;
    }

}