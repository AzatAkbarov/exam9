<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FavoriteRepository")
 */
class Favorite
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="favorite_posts")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $dataRedditForPag;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created_date;


    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $postId;

    /**
     * @param int $id
     * @return Favorite
     */
    public function setId(int $id): Favorite
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param User $user
     * @return Favorite
     */
    public function setUser(User $user): Favorite
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param \DateTime $created_date
     * @return Favorite
     */
    public function setCreatedDate(\DateTime $created_date): Favorite
    {
        $this->created_date = $created_date;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->created_date;
    }

    /**
     * @param string $postId
     * @return Favorite
     */
    public function setPostId(string $postId): Favorite
    {
        $this->postId = $postId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostId(): string
    {
        return $this->postId;
    }

    /**
     * @param mixed $dataRedditForPag
     * @return Favorite
     */
    public function setDataRedditForPag($dataRedditForPag)
    {
        $this->dataRedditForPag = $dataRedditForPag;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataRedditForPag()
    {
        return $this->dataRedditForPag;
    }

}