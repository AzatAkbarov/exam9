<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Post\PostHandler;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @param ApiContext $apiContext
     * @param PostHandler $postHandler
     * @return Response
     * @throws \App\Model\Api\ApiException
     */
    public function indexAction(Request $request, ApiContext $apiContext, PostHandler $postHandler)
    {
        $form = $this->createForm('App\Form\SearchType');
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $array = array_merge($data, ['type' => 'link']);
            $result = $apiContext->getRedditPosts($array);
            $posts = $postHandler->createPosts($result);
            return $this->render('index.html.twig', [
                'form' => $form->createView(),
                'posts' => $posts
            ]);
        }
        return $this->render('index.html.twig', [
            'form' => $form->createView(),
            'posts' =>  null
        ]);
    }

    /**
     * @Route("/authentication", name="authentication")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function userAuthenticationAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler
    )
    {

        $form = $this->createForm("App\Form\LoginType");

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $email = $data['email'];
            $password = $data['password'];

            /** @var User $user || null */
            $user = $userRepository->findByPasswordAndEmail($email, $password);

            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('homepage');
            } else {
                $error = 'Вас нету в базе,зарегистрируйтесь';
            }
        }
        return $this->render('authentication.html.twig', array(
            "form" => $form->createView(),
            'error' => $error
        ));
    }

    /**
     * @Route("/registration", name="registration")
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function userRegistrationAction(
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = [
                'email' => $user->getEmail(),
                'password' => $user->getPassword()
            ];

            $user = $userHandler->createNewUser($data);
            $manager->persist($user);
            $manager->flush();

            $userHandler->makeUserSession($user);
            return $this->redirectToRoute('homepage');

        }
        return $this->render('registration.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }
}