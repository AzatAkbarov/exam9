<?php

namespace App\Controller;

use App\Entity\Favorite;
use App\Model\Api\ApiContext;
use App\Model\Post\PostHandler;
use App\Repository\FavoriteRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/posts")
 */
class PostController extends Controller
{
    /**
     * @Route("/{name}/details", name="details")
     * @param ApiContext $apiContext
     * @param PostHandler $postHandler
     * @param $name
     * @return Response
     * @throws \App\Model\Api\ApiException
     */
    public function detailsAction(ApiContext $apiContext, PostHandler $postHandler, $name)
    {
        $data = $apiContext->getOnePost($name);
        $post = $postHandler->createPostForViewDetails($data);
        return $this->render('post_details.html.twig', [
            'post' =>  $post
        ]);
    }

    /**
     * @Route("/show_my_posts", name="show_my_posts")
     * @param ApiContext $apiContext
     * @param PostHandler $postHandler
     * @param FavoriteRepository $favoriteRepository
     * @return Response
     * @throws \App\Model\Api\ApiException
     */
    public function showMyFavoritePostAction(ApiContext $apiContext, PostHandler $postHandler, FavoriteRepository $favoriteRepository, Request $request)
    {
        $currentUser = $this->getUser();
        /** @var Favorite[] $myFavoritesPosts */
        $myFavoritesPosts = $favoriteRepository->findMyFavoritesPosts($currentUser->getId());

        $string = null;
        foreach ($myFavoritesPosts as $favorite) {
            $string .= $favorite->getPostId().',';
        }
        $postsFromReddit = $apiContext->getFavoritePosts($string);

        $resPosts = $postHandler->createPosts($postsFromReddit);

        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt("page", 1);
        $posts = $paginator->paginate(
            $resPosts,
            $page,
            4
        );
        return $this->render('my_favorite_posts.html.twig', [
            'posts' =>  $posts
        ]);
    }

    /**
     * @Route("/show_all_favorites_posts", name="show_all_favorites_posts")
     * @param ApiContext $apiContext
     * @param PostHandler $postHandler
     * @param FavoriteRepository $favoriteRepository
     * @return Response
     * @throws \App\Model\Api\ApiException
     */
    public function showAllFavoritePostsAction(ApiContext $apiContext, PostHandler $postHandler, FavoriteRepository $favoriteRepository, Request $request)
    {
        /** @var Favorite[] $allFavoritesPosts */
        $allFavoritesPosts = $favoriteRepository->findAllFavoritesPosts();

        $string = null;
        foreach ($allFavoritesPosts as $favorite) {
            $string .= $favorite['postId'].',';
        }
        $postsFromReddit = $apiContext->getFavoritePosts($string);

        $resPosts = $postHandler->createPosts($postsFromReddit);

        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt("page", 1);
        $posts = $paginator->paginate(
            $resPosts,
            $page,
            4
        );
        return $this->render('show_all_favorites_posts.html.twig', [
            'posts' =>  $posts
        ]);
    }

    /**
     * @Route("/add_to_favorite/{name}", name="add_to_favorite")
     * @param $name
     * @param ObjectManager $manager
     * @param Request $request
     * @param FavoriteRepository $favoriteRepository
     * @param Session $session
     * @return Response
     */
    public function addToFavoritePosts($name, ObjectManager $manager, Request $request, FavoriteRepository $favoriteRepository, Session $session)
    {
        $currentUser = $this->getUser();
        if(!$currentUser) {
            return $this->redirectToRoute('homepage');
        } else {
            $post = $favoriteRepository->findOneByNameAndUserId($name, $currentUser->getId());
            if($post) {
                $session->getFlashBag()->set('error', 'Вы уже добавляли этот пост в избранное');
                return $this->redirect(
                    $request
                        ->headers
                        ->get('referer')
                );
            } else {
                $favoritePost = new Favorite();
                $favoritePost->setPostId($name)
                    ->setUser($this->getUser())
                    ->setCreatedDate( new \DateTime());
                $manager->persist($favoritePost);
                $manager->flush();
                $session->getFlashBag()->set('message', 'Вы успешно добавили этот пост в избранные');
                return $this->redirect(
                    $request
                        ->headers
                        ->get('referer')
                );
            }

        }
    }
}