<?php

namespace App\Repository;

use App\Entity\Favorite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Favorite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Favorite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Favorite[]    findAll()
 * @method Favorite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavoriteRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Favorite::class);
    }

    public function countUserLiker($name)
    {
        return $this->createQueryBuilder('f')
            ->select('count(f)')
            ->where('f.postId = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getResult();
    }

    public function findOneByNameAndUserId($name, $userId)
    {
        try {
            return $this->createQueryBuilder('f')
                ->select('f')
                ->where('f.postId = :name')
                ->andWhere('f.user = :userId')
                ->setParameter('name', $name)
                ->setParameter('userId', $userId)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findAllFavoritesPosts()
    {
        return $this->createQueryBuilder('f')
            ->select('f.postId, COUNT(f.postId) AS HIDDEN b')
            ->groupBy('f.postId')
            ->orderBy('b', "desc")
            ->getQuery()
            ->getResult();
    }

    public function findMyFavoritesPosts($userId)
    {
        return $this->createQueryBuilder('f')
            ->select('f')
            ->where('f.user = :userId')
            ->setParameter('userId', $userId)
            ->orderBy('f.created_date', 'DESC')
            ->getQuery()
            ->getResult();
    }

}