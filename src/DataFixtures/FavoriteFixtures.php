<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/7/18
 * Time: 8:11 PM
 */

namespace App\DataFixtures;


use App\Entity\Favorite;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class FavoriteFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $arrayOfNamesPosts = ['t3_8dalmv','t3_8hi3r7','t3_87a9dz','t3_8qu4fz','t3_85l453','t3_8wt3ey','t3_8wt5p6','t3_8wt3qi'];


        for($i = 0;$i < 8; $i++ ) {
            $favorite = new Favorite();
            $favorite->setPostId($arrayOfNamesPosts[$i])
                ->setUser($this->getReference(UserFixtures::USER_ONE))
                ->setCreatedDate( new \DateTime());
            $manager->persist($favorite);
            $manager->flush();
        }

        for($i = 0; $i < 8; $i++) {
            $favorite = new Favorite();
            $favorite->setPostId($arrayOfNamesPosts[$i]);
            if($i < 4) {
                $favorite->setUser($this->getReference(UserFixtures::USER_TWO));
            } else {
                $favorite->setUser($this->getReference(UserFixtures::USER_THREE));
            };

            $favorite->setCreatedDate( new \DateTime());
            $manager->persist($favorite);
            $manager->flush();
        }

    }
    function getDependencies()
    {
        return array(
            UserFixtures::class
        );
    }
}
