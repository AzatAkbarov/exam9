<?php

namespace App\DataFixtures;


use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public const USER_ONE = 'asd@asd.asd';
    public const USER_TWO = 'qwe@qwe.qwe';
    public const USER_THREE = 'zxc@zxc.zxc';

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    public function load(ObjectManager $manager)
    {
        $user1 = $this->userHandler->createNewUser([
            'email' => 'asd@asd.asd',
            'password' => '213471118',
        ]);

        $manager->persist($user1);

        $user2 = $this->userHandler->createNewUser([
            'email' => 'qwe@qwe.qwe',
            'password' => '1123581321',
        ]);

        $manager->persist($user2);

        $user3 = $this->userHandler->createNewUser([
            'email' => 'zxc@zxc.zxc',
            'password' => '32571219',
        ]);

        $manager->persist($user3);
        $manager->flush();

        $this->addReference(self::USER_ONE, $user1);
        $this->addReference(self::USER_TWO, $user2);
        $this->addReference(self::USER_THREE, $user3);

    }
}