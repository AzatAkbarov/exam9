<?php

namespace App\Model\Api;

class ApiContext extends AbstractApiContext
{

    /**
     * @param array
     * @return mixed
     * @throws ApiException
     */
    public function getRedditPosts($array)
    {
       return $this->makeQuery('https://www.reddit.com/r/picture/search.json', self::METHOD_GET, $array);
    }

    /**
     * @param array
     * @return mixed
     * @throws ApiException
     */
    public function getOnePost($name)
    {
        return $this->makeQuery('https://www.reddit.com/api/info.json', self::METHOD_GET, ['id' => $name ]);
    }


    /**
     * @param string
     * @return mixed
     * @throws ApiException
     */
    public function getFavoritePosts($string)
    {
        return $this->makeQuery('https://www.reddit.com/api/info.json', self::METHOD_GET, ['id' => $string ]);
    }
}