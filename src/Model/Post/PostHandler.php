<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7/7/18
 * Time: 1:49 PM
 */

namespace App\Model\Post;


use App\Entity\Post;
use App\Repository\FavoriteRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class PostHandler
{

    private $container;
    private $favoriteRepository;

    public function __construct(ContainerInterface $container, FavoriteRepository $favoriteRepository)
    {
        $this->container = $container;
        $this->favoriteRepository = $favoriteRepository;
    }

    /**
     * @param array $data
     * @return Post[]
     */
    public function createPosts(array $data) {
        $posts = [];
        $children = $data['data']['children'];
        foreach ($children as $value) {
            $preview = $value['data']['thumbnail'] ?? null;
            $title = $value['data']['title'];
            $author = $value['data']['author'];
            $date = date('d-m-Y',($value['data']['created_utc'] ?? time() ));
            $name =  $value['data']['name'];
            $userQuantity = $this->favoriteRepository->countUserLiker($name);
            $data = [
                'id' => $name,
                'title' => $title,
                'author' => $author,
                'date' => $date,
                'image' => $preview,
                'quantity' => $userQuantity
            ];
            $post = $this->createPost($data);
            $posts[] = $post;
        }
        return $posts;
    }

    /**
     * @param array $array
     * @return Post
     */
    public function createPostForViewDetails($array) {
        $children = $array['data']['children'];
        $value = $children[0]['data'];
        $preview = $value['url'] ?? null;
        $title = $value['title'];
        $author = $value['author'];
        $date = date('d-m-Y',($value['created_utc'] ?? time() ));
        $name =  $value['name'];
        $userQuantity = $this->favoriteRepository->countUserLiker($name);
        $data = [
            'id' => $name,
            'title' => $title,
            'author' => $author,
            'date' => $date,
            'image' => $preview,
            'quantity' => $userQuantity
        ];
        $post = $this->createPost($data);
        return $post;
    }
    /**
     * @param array $data
     * @return Post
     */
    public function createPost(array $data) {
            $post = new Post();
            $post->setId($data['id'])
                ->setImage($data['image'])
                ->setAuthor($data['author'])
                ->setTitle($data['title'])
                ->setCreatedUts($data['date'])
                ->setUserQuantity($data['quantity']);
        return $post;
    }
}